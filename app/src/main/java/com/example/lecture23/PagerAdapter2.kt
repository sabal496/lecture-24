package com.example.lecture23

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.pager_two.view.*

class PagerAdapter2(): PagerAdapter() {

    var imglist= mutableListOf<Int>()

    override fun getCount():Int=imglist.size

    override fun isViewFromObject(view: View, itemiew: Any): Boolean {
       return view==itemiew

     }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
         val itemview=LayoutInflater.from(container.context).inflate(R.layout.pager_two,container,false)
        container.addView(itemview)
        itemview.position.text=position.toString()
        itemview.imagerec.setImageResource(imglist[position])
        return itemview
    }

    override fun destroyItem(container: ViewGroup, position: Int, itemiew: Any) {
        container.removeView(itemiew as View)
    }



}