package com.example.lecture23

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private  fun init(){

        var imagelist= mutableListOf<Int>()
        imagelist.add(R.mipmap.c)
        imagelist.add(R.mipmap.kotlin)
        imagelist.add(R.mipmap.python)
        imagelist.add(R.mipmap.ruby)
        imagelist.add(R.mipmap.swift)

       /* val adapter=PagerAdapterFragment(supportFragmentManager,1)
        adapter.imglist=imagelist
        viewpager.adapter=adapter
*/


        val adapter=PagerAdapter2()
        adapter.imglist=imagelist
        viewpager.adapter=adapter

        add.setOnClickListener(){
            var randomitempos:Int=(0 until imagelist.size).random()
            imagelist.add(imagelist[randomitempos])
            adapter.notifyDataSetChanged()
        }

    }


}
