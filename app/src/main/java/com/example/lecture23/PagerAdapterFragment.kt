package com.example.lecture23

import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class PagerAdapterFragment(fm :FragmentManager, behavior:Int):FragmentStatePagerAdapter(fm,behavior) {

    var imglist= mutableListOf<Int>()

    override fun getItem(position: Int): Fragment {
        return   PagerFragment(imglist[position],position)
     }

    override fun getCount(): Int =imglist.size
}